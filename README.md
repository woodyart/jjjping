# JJJPing

This is standalone application to test GET and POST written on Java with using Spring.
It has a built-in Tomcat server running on port 8181. In this case jjjping supports only x64 OS (Tested on CentOS 7 and Ubuntu 14.04).

## Full install JJJPing

1. Download jjjping-<version>-bin.tar.gz from release folder
2. Extract archieve:
    $ tar xzf jjjping-<version>-bin.tar.gz -C /path/to/any/folder
3. Go to your jjjping folder and run:
    # ./install.sh jjjping-<VERSION_HERE>.jar

## Run

After install jjjping-service runs automatically. You can start|stop|restart jjjping:

    # service jjjping start

Also you can run jjjping as application. Just type in terminal:

    $ jjjping

Now open localhost:8181/ping and write something. Write "ping", to see extra result

You can also check result with command:

    $ curl -d "content=ping&submit=Submit" localhost:8181/ping 2>/dev/null | grep -o -P '(?<=<p>).*(?=</p>)'

## Logs

If your system uses system V (Ubuntu 14.04), see logs here:

     /var/log/jjjping.log

If your system uses systemd (CentOS 7), see logs with help journalctl tool:

    $ jpurnalctl -u jjjping

## Build from sources

1. Install [Maven](https://maven.apache.org/)
2. install [Spring](http://spring.io/)
3. Download repo and cd into project folder
4. Run command:
    $ mvn clean package assembly:single

## Known bugs

### Tomcat starts slowly.

It's because Tomcat has bug - [JDK-4705093](http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4705093)

    2015-12-13 02:48:29.285  INFO 4910 --- [ost-startStop-1] o.a.c.util.SessionIdGeneratorBase        : Creation of SecureRandom instance for session ID generation using [SHA1PRNG] took [166,611] milliseconds.

FIX: add to JAVA_OPTS into "conf" file next parameter:

    -Djava.security.egd=file:/dev/./urandom
