package com.woody;

import java.util.Date;

public class Ping {

    private String content;
    private String input = "ping";
    private String output = "pong";

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        if (content.equals(this.input)) {
             Date d = new Date();
             this.content = this.output+" - "+d;
        } else {
             this.content = content;
        }
    }

}
