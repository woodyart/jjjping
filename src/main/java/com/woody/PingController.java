package com.woody;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PingController {

    @RequestMapping(value="/ping", method=RequestMethod.GET)
    public String pingForm(Model model) {
        model.addAttribute("ping", new Ping());
        return "ping";
    }

    @RequestMapping(value="/ping", method=RequestMethod.POST)
    public String pingSubmit(@ModelAttribute Ping ping, Model model) {
        model.addAttribute("ping", ping);
        return "result";
    }

}
