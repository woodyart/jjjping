#!/bin/bash
# This script will install jjjping as application and as service
if [ -z "$1" ];
then
  echo "Please, use ./$(basename -- "$0") jjjping.jar"
  exit 1
fi

if [ ! -f $1 ];
then
  echo "file $1 doesn't exist"
  exit 1
fi

RELEASE_NAME=$1
RELEASE_PATH=$(readlink -f $1)
RELEASE_CONF_FILE=${RELEASE_NAME/jar/conf}
APP_NAME="jjjping"
APP_PATH="/usr/bin"
INIT_SYSTEM=$(sudo stat /proc/1/exe | awk /exe/'{print $4}')
INIT_NAME="jjjping"
INIT_PATH="/etc/init.d"
SYSTEMD_NAME="jjjping.service"
SYSTEMD_PATH="/etc/systemd/system/"
JAVA_HOME="$(pwd)/jre-x64"

# Creating conf file
if [ -f $RELEASE_CONF_FILE ];
then
  echo "Founded conf file $RELEASE_CONF_FILE, replacing..."
  truncate -s0 $RELEASE_CONF_FILE
else
  echo "Creating conf file $RELEASE_CONF_FILE..."
  touch $RELEASE_CONF_FILE
fi
echo "JAVA_HOME=$JAVA_HOME"     >> $RELEASE_CONF_FILE
echo 'JAVA_OPTS="-Djava.security.egd=file:/dev/./urandom -Djava.awt.headless=true -Xmx1024m -XX:MaxPermSize=512m -XX:+UseConcMarkSweepGC"' >> $RELEASE_CONF_FILE
echo "LOG_FOLDER=/var/log"      >> $RELEASE_CONF_FILE
echo "LOG_FILENAME=$APP_NAME"   >> $RELEASE_CONF_FILE
echo "Done"

# Creating app in /usr/bin
if [ -f $APP_PATH/$APP_NAME ];
then
  echo "Founded $APP_NAME in $APP_PATH, updating"
  truncate -s0 $APP_PATH/$APP_NAME
else
  echo "Creating $APP_NAME in $APP_PATH"
  touch $APP_PATH/$APP_NAME
  chmod +x $APP_PATH/$APP_NAME
fi
echo "$RELEASE_PATH" >> $APP_PATH/$APP_NAME
echo "Done"

# Creating service
# if system V
echo $INIT_SYSTEM | grep init > /dev/null
if [ $? -eq 0 ];
then
  if [ -f $INIT_PATH/$INIT_NAME ];
  then
    echo "Service $INIT_NAME already exist, updating"
    $INIT_PATH/$INIT_NAME stop &>/dev/null
    if [ $? -ne 0 ];
    then
        #sleep 1s
        kill $(ps aux | grep jjjping*jar | grep -v grep | awk '{print $2}')
    fi
    update-rc.d -f $INIT_NAME remove &>/dev/null
    rm $INIT_PATH/$INIT_NAME
  else
    echo "Creating service $INIT_NAME"
  fi
  ln -s "$RELEASE_PATH" $INIT_PATH/$INIT_NAME
  update-rc.d $INIT_NAME defaults &>/dev/null
  echo "Done"
  echo "Starting service"
  $INIT_PATH/$INIT_NAME start &>/dev/null
  echo "Done"
else
  # if systemd
  echo $INIT_SYSTEM | grep systemd > /dev/null
  if [ $? -eq 0 ];
  then
    if [ -f $SYSTEMD_PATH/$SYSTEMD_NAME ];
    then
      echo "Service $SYSTEMD_NAME already exist, updating"
      $(which systemctl) disable $SYSTEMD_NAME &>/dev/null
      $(which systemctl) stop $SYSTEMD_NAME &>/dev/null
      sleep 1s
      truncate -s0 $SYSTEMD_PATH/$SYSTEMD_NAME
    else
      echo "Creating service $SYSTEMD_NAME"
      touch $SYSTEMD_PATH/$SYSTEMD_NAME
    fi
    echo "[Unit]"                     >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo "Description=$INIT_NAME"     >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo "After=syslog.target"        >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo ""                           >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo "[Service]"                  >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo "User=root"                  >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo "ExecStart=$RELEASE_PATH"    >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo "SuccessExitStatus=143"      >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo ""                           >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo "[Install]"                  >> $SYSTEMD_PATH/$SYSTEMD_NAME
    echo "WantedBy=multi-user.target" >> $SYSTEMD_PATH/$SYSTEMD_NAME

    $(which systemctl) enable $SYSTEMD_NAME &>/dev/null
    echo "Done"
    echo "Starting service"
    $(which systemctl) start $SYSTEMD_NAME &>/dev/null
    echo "Done"
  else
    echo "Unsupported init system, sorry"
    exit 1
  fi
fi

exit 0
