/**
 * Before run, install:
 * sdkman from here - http://sdkman.io
 * srpingboot with this comand - sdk install springboot
 *
 * run this with command:
 * spring run testmyinstallation.groovy 
 * 
 * and look for result:
 * curl -s localhost:8080
 */

@RestController
class ThisWillActuallyRun {

    @RequestMapping("/")
    String home() {
        "Hello World!"
    }
    @RequestMapping("/ping")
    String ping() {
	def date = new Date()
	"Pong!"+date
    }

}
